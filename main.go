package main

import (
	"aws-golang/dal"
	"bitbucket.org/swigy/dal-go-commons/commons"
	"bitbucket.org/swigy/dal-go-commons/kvstorev2"
	"fmt"
	"github.com/spf13/viper"
	"reflect"
	"time"
)

type Config struct {
	listenHost string
	listenPort string
	awsRegion  string
	// If the DAL providers (e.g DDB, SDB) are running locally, their endpoint configuration
	// (format http://host:port)
	localDALEndpoint string
	redisAddress     string
	tableNamePrefix  string
	logPath          string
	logLevel         string
	marketPlaceID    string
	daxEndpoint      string //http://hostport
	// config related to AWS lambda for disable offer
	disableOfferLambdaARN string
	cwRoleARN             string
	cwEventPrefix         string
	cwEventsEnabled       bool
}

func GetSessionConfig(config Config) kvstorev2.SessionConfig {
	sessionConfig := kvstorev2.SessionConfig{
		MaxRetries: commons.IntPtr(7),
		RetryDelay: 10 * time.Millisecond,
		AWSRegion:  config.awsRegion,
		DDBSessionConfig: &kvstorev2.DdbSessionConfig{
			DDBEndpoint: config.localDALEndpoint,
		},
	}
	return sessionConfig
}

func main() {
	viper.AutomaticEnv()
	viper.SetConfigType("yml")
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	config := Config{
		listenHost:            viper.GetString("LISTEN_HOST"),
		listenPort:            viper.GetString("LISTEN_PORT"),
		awsRegion:             viper.GetString("AWS_REGION"),
		localDALEndpoint:      viper.GetString("LOCAL_DAL_ENDPOINT"),
		tableNamePrefix:       viper.GetString("TABLE_NAME_PREFIX"),
		logPath:               viper.GetString("LOG_PATH"),
		logLevel:              viper.GetString("LOG_LEVEL"),
		marketPlaceID:         viper.GetString("MARKETPLACE_ID"),
		daxEndpoint:           viper.GetString("DAX_ENDPOINT"),
		disableOfferLambdaARN: viper.GetString("DISABLE_OFFER_LAMBDA_ARN"),
		cwRoleARN:             viper.GetString("CLOUD_WATCH_ROLE_ARN"),
		cwEventPrefix:         viper.GetString("CW_EVENT_PREFIX"),
		cwEventsEnabled:       viper.GetBool("CE_ENABLED"),
	}
	
	dal.Init(GetSessionConfig(config))

    dal.CreateTable()

	item := dal.TestItem{
		Name: "Test For Multi Attribute Put 1",
		Action:dal.PUT,
		CompositeKey:kvstorev2.CompositeKey{
			PartitionKey:kvstorev2.Key{Kind:reflect.String,  Name: "partition_id", Type:kvstorev2.HASH, Data: "partition_id_1"},
			SortKey:kvstorev2.Key{Kind:reflect.String,  Name: "sort_key_id", Type:kvstorev2.RANGE, Data: "sort_key_id_1"},
		},
		Attributes: map[string]kvstorev2.Value{
			"attr_1": {Kind:reflect.Int, Data: 100},
			"attr_2" : {Kind:reflect.String, Data: "attr_2_value"},
			"attr_3":{Kind:reflect.Slice, Data:[]string{"attr_3_val_1","attr_3_val_2", "attr_3_val_3"}},
			"attr_4": {Kind:reflect.Slice, Data: []int{101,102,103}}},
	}

	dal.PutAttributes(item)

}
