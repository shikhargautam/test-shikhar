module aws-golang

go 1.13

require (
	bitbucket.org/swigy/dal-go-commons v0.4.6
	github.com/aws/aws-sdk-go v1.25.18 // indirect
	github.com/prometheus/common v0.6.0
	github.com/spf13/viper v1.5.0
)
