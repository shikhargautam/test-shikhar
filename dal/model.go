package dal

import (
	"bitbucket.org/swigy/dal-go-commons/kvstore"
	_ "reflect"
	_ "strings"
)

type TableType int

const (
	Campaign TableType = iota
)

const (
	KeySuffix   = "ID"
	ValueSuffix = "Data"
	Version     = "version"
)

type TableConfig struct {
	TableName string
	ValueName string
	KeyName   string
	Keys      []kvstore.Key
}

type TableDefs map[TableType]TableConfig

//func ComposeTableDefinitions() TableDefs {
//	//defs := TableDefs{}
//	//keyName := strings.ToLower("game" + KeySuffix)
//	//valueName := strings.ToLower("game" + ValueSuffix)
//	//tableName := strings.ToLower("game")
//	//var keys []kvstore.Key
//	//keys = append(keys, kvstore.Key{
//	//	Name: keyName,
//	//	Kind: reflect.String,
//	//	Type: kvstore.HASH,
//	//})
//	//defs[0] = TableConfig{
//	//	TableName: tableName,
//	//	KeyName:   keyName,
//	//	Keys:      keys,
//	//	ValueName: valueName,
//	//}
//	//return defs
//
//}