package dal

import (
	"bitbucket.org/swigy/dal-go-commons/kvstorev2"
	"context"
	"errors"
	"fmt"
	_ "github.com/prometheus/common/log"
	"reflect"
)

var provider kvstorev2.Provider
var compositeKey = kvstorev2.CompositeKey{
	PartitionKey:kvstorev2.Key{Kind:reflect.String,  Name: "partition_id", Type:kvstorev2.HASH},
	SortKey:kvstorev2.Key{Kind:reflect.String,  Name: "sort_key_id", Type:kvstorev2.RANGE},
	Ttl: 100000,
}
var tableName = "Game";

func Init(config kvstorev2.SessionConfig) error {
	session, err := kvstorev2.NewSession(&config)
	if err != nil {
		return errors.New(fmt.Sprintf("cannot create a KV session: %v", err))
	}

	provider = session
	return nil
}

func CreateTable()  {
	provider.CreateTable(tableName, compositeKey)
}


type TableAction int

const (
	PUT TableAction = iota
	GET
	UPDATE
	REMOVE
	BATCH_WRITE
	BATCH_GET
	DELETE
	REMOVE_FROM_SET
	ADD_TO_SET
	REMOVE_FROM_MAP
	ADD_TO_MAP
	INCR_VALUE
	DECR_VALUE

)

type TestItem struct {
	Name string
	Action TableAction
	CompositeKey  kvstorev2.CompositeKey
	AttributeNames []string
	Attributes map[string]kvstorev2.Value
	MapAddAttributes map[string]map[string]kvstorev2.Value
	MapRemoveAttributes map[string][]string
	BatchPutInput  []*kvstorev2.BatchPutInput
	BatchGetInput  []*kvstorev2.BatchGetInput
	BatchPutOutput []*kvstorev2.BatchPutOutput
	BatchGetOutput []*kvstorev2.BatchGetOutput
}



func PutAttributes(item TestItem) error {
	attributes := item.Attributes
	if attributes == nil {
		return errors.New("attributes is not defined for put operation")
	}
	err := provider.PutWithContext(context.Background(), tableName, item.CompositeKey, attributes)
	return err
}



